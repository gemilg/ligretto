package pl.wroc.pwr.ligretto.game;

import pl.wroc.pwr.ligretto.model.Card;
import pl.wroc.pwr.ligretto.model.CardSet;
import pl.wroc.pwr.ligretto.model.CardStack;
import pl.wroc.pwr.ligretto.model.Player;
import pl.wroc.pwr.ligretto.model.Table;

public class Game {

	private CardSet cardSet;
	private Player[] players;
	private int maxPlayers = 4;
	private int numberOfPlayers = 0;
	
	public Game() {
		System.out.println("Game()");
		cardSet = new CardSet();
	}
	
	public static void main(String[] args) {
		Game game = new Game();
		game.addPlayer(new Player());
		game.players[0].set(1);
		
		game.addPlayer(new Player());
		game.players[1].set(2);
		
		game.addPlayer(new Player());
		game.players[2].set(3);
		
		game.addPlayer(new Player());
		game.players[3].set(4);
		
		Card card = new Card();
		card.symbol = 1;
		card.color =1;
		card.number = 4;
		
		Table table = new Table();// utworzenie stołu z 16 stosami
		
		game.play();
	}
	
	public CardSet getCardSet() {
		return cardSet;
	}

	public Player[] getPlayers() {
		return players;
	}

	public void play() {
		System.out.println("Welcome to Ligretto!");
	}

	public void addPlayer(Player player) {
		Player[] temp = new Player[numberOfPlayers+1];
		
		for(int i=0; i<numberOfPlayers; i++){
			temp[i]=players[i];
		}
		temp[numberOfPlayers]=player;
		players = temp;
		numberOfPlayers++;
		System.out.println("Player "+numberOfPlayers+" created");
	}
	
}
