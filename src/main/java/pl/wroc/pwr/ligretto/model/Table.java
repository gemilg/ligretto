package pl.wroc.pwr.ligretto.model;

public class Table {
	public CardStack[] cardStack;
	public Table(){
		cardStack = new CardStack[16];//na stole może być 16 stosów
	}
	
	public CardStack getStack(int i){
		return cardStack[i];
	}
	public boolean addCard(Card card, int i){//sprawdzenie czy można dodać karte do stosu, jeśli tak to dodaje
		if(cardStack[i].isEmpty() && card.number == 1){
			cardStack[i].addCard(card);
			cardStack[i].color = card.color;
			cardStack[i].number = 1;
			return true;
		}
		else if(!cardStack[i].isEmpty() && card.color == cardStack[i].color && card.number==cardStack[i].number-1){
			cardStack[i].addCard(card);
			cardStack[i].number=card.number;
			return true;
		}
		else return false;
	}
}
