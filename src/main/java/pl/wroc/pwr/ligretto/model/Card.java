package pl.wroc.pwr.ligretto.model;

public class Card implements Comparable<Card> {
	public int symbol;
	public int number;
	public int color;
	@Override
	public String toString(){
		return "card "+symbol+" "+number+" "+color;
	}
	public int compareTo(Card o) {
		return 0;
	}

}
