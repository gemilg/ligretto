package pl.wroc.pwr.ligretto.model;

public class Player {
	public CardStack ligrettoStack = new CardStack();
	public Row row = new Row();
	public CardStack hand = new CardStack();

	public int symbol;
	
	public void set(int s){
		symbol = s;
		hand.fillStack(symbol);
		hand.Shuffle();
		for(int i=0; i<ligrettoStack.maxCards; i++)ligrettoStack.addCard(hand.removeFromTop());//dodanie 10 kart z ręki do stosu ligretto
		for(int i=0; i<row.maxCards; i++)row.addCard(hand.removeFromTop());//dodan
	}
	public Card[] getCards() {
		// TODO Auto-generated method stub
		return null;
	}
	public Card getCardFromStack(){
		return ligrettoStack.removeFromTop();
	}
	public Card getCardFromRow(int i){
		return row.removeCard(i);
	}
	public Card getCardFromHand(){
		return hand.removeFromTop();
	}
	public void makeAMove(){
		
	}
}
