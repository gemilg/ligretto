package pl.wroc.pwr.ligretto.model;
import java.util.Collections;
import java.util.ArrayList;
import java.util.List;

public class CardStack {
	List<Card> cardStack = new ArrayList<Card>();
	int maxCards = 10;
	int color;//pomocniczy kolor
	int number;//pomocniczy numer na wierzchu stosu
	public void addCard(Card card){
		cardStack.add(card);
	}
	public void Shuffle(){
		Collections.shuffle(cardStack);
	}
	public Card removeFromTop(){
		return cardStack.remove(cardStack.size()-1);
	}

	public void fillStack(int s){
		for(int i=0; i<10; i++){
			for(int j=0; j<4; j++){
				Card temp = new Card();
				temp.symbol = s;
				temp.number = i+1;
				temp.color = j+1;
				addCard(temp);
			}
		}
	}
	public boolean isEmpty(){
		if(cardStack.size()==0)return true;
		else return false;
	}
}
